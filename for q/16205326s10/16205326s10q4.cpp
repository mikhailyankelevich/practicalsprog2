//
//  16205326s10q4.cpp
//
//student info in class program
//  Created by Mikhail Yankelevich on 19/04/2018.


#include <iostream>
#include <string>
using namespace std;


class Student {//initializing class consisting of set functions and get functions for each peivate variubles + stage increase function and gpa increse + decrese function
private:
    string name="NONE";
    string address="NONE";
    int phoneNumber=0;
    string fieldOfStuddy="NONE";
    int stage=0;
    double GPA=0;

public:
    void setName(string str){
        name =str;
    }
    string getName()
    {
        return name;
    }
    void setAddress(string str){
        address =str;
    }
    string getAddress()
    {
        return address;
    }
    void setFieldOfStuddy(string str){
        fieldOfStuddy =str;
    }
    string getFieldOfStuddy()
    {
        return fieldOfStuddy;
    }
    void setPhoneNumber(int numb){
        phoneNumber =numb ;
    }
    int getPhoneNumber(){
        return phoneNumber;
    }
    void setStageNumber(int numb){
        stage =numb ;
    }
    int getStageNumber(){
        return stage;
    }
    void setGPA(double numb){
        GPA =numb ;
    }
    double getGPA(){
        return GPA;
    }
    int increaseStage(){
        stage++;
        return stage;
    }
    double increaseGPA(double num){
        if (num+GPA<4.2)
        {
            GPA=GPA+num;
        }
        else
        {
            return 0;
        }
        return GPA;
    }
    double decreaseGPA(double num){
        if (GPA-num>0)
        {
            GPA-=num;
        }
        else
        {
            return 0;
        }
        return GPA;
    }

};



void GPACheck(Student student)//checking if gpa >2 if not prinitg warning
{
    if (student.getGPA()<2)
        cout<<"!!!!!!GPA is too low ("<<student.getGPA()<<")!!!!!!!"<<endl;
    
}

void printStudentData(Student student){//printing all student data ( prints warning if the GPA is too low)
    cout<<"Name "<<student.getName()<<endl;
    cout<<"Address "<<student.getAddress()<<endl;
    cout<<"Phone number "<<student.getPhoneNumber()<<endl;
    cout<<"Field of studdy "<<student.getFieldOfStuddy()<<endl;
    cout<<"Stage number "<<student.getStageNumber()<<endl;
    cout<<"GPA "<<student.getGPA()<<endl;
    GPACheck(student);
    cout<<endl;
    cout<<endl;
    cout<<endl;
}




Student addInfo(Student student)//addign into about the student
{
    string name;
    string address;
    int phoneNumber;
    string fieldOfStuddy;
    int stage;
    double GPA;
    
    cout<<"to add/modify name enter 1\nto add/modify address enter 2\nto add/modify phone number enter 3\nto add/modify Field of studdy enter 4\nto add/modify stage enter 5\nto add/modify GPA enter 6\nto stop modifying enter 0"<<endl;
    int i;
    cin>>i;//checking what to add
    cout<<endl;
    while (i!=0)//while 0 was not entered scan values and write them in class
    {
        if (i==1)
        {
            cout<<"enter name of the student"<<endl;
            getline( cin, name );
            getline( cin, name );
            cout<<endl;
            student.setName(name);//record in class
        }
        else if (i==2)
        {
            cout<<"enter an address of the student"<<endl;
            getline( cin, address );
            getline(cin, address);
            cout<<endl;
            student.setAddress(address);
        }
        else if (i==3)
        {
            cout<<"enter a phone number of the student"<<endl;
            cin>>phoneNumber;
            cout<<endl;
            student.setPhoneNumber(phoneNumber);
        }
        else if (i==4)
        {
            cout<<"enter a field of studdy of the student"<<endl;
            getline(cin, fieldOfStuddy );
            getline(cin, fieldOfStuddy);
            cout<<endl;
            student.setFieldOfStuddy(fieldOfStuddy);
        }
        else if (i==5)
        {
            cout<<"enter a stage number of the student"<<endl;
            cin>>stage;
            cout<<endl;
            student.setStageNumber(stage);
        }
        else if ( i==6)
        {
            cout<<"enter a GPA of the student"<<endl;
            cin>>GPA;
            cout<<endl;
            student.setGPA(GPA);
        }
        cout<<"to add/modify name enter 1\nto add/modify address enter 2\nto add/modify phone number enter 3\nto add/modify Field of studdy enter 4\nto add/modify stage enter 5\nto add/modify GPA enter 6\nto stop modifying enter 0"<<endl;
        cin>>i;
        cout<<endl;
        
    }
            printStudentData(student);//print all student data
    
    return student;//returning the class( in order to update it, didm't update whith any other method)
}






Student studentInfo(Student student)//menu on every manipulation which could be done whith the student class
{
    
    int i;
    cout<<"to add info enter 1\nto print info enter 2\nto change stage enter 3\nto change GPA enter 4\nto end enter 0"<<endl;
    GPACheck(student);//checking if student GPA is high enough
    cin>>i;//scannig wht to pik
    cout<<endl;
    while (i!= 0)//while 0 isn't entered picking wht to do with student sruct
    {
        if (i==1)
           student=addInfo(student);
        else if (i==2)
            printStudentData(student);
        else if (i==3)
            cout<<"stage is increased by 1 year and now is "<< student.increaseStage()<<endl;
        else if (i==4)
        {
            cout<<"to increasy GPA by some value enter 1 , else enter 2"<<endl;
            int j;
            double increse;
            cin>>j;
            if (j==1)
            {
                cout<<"How much to increse the GPA?"<<endl;
                cin>>increse;
                double GPA=student.increaseGPA(increse);
                if (GPA==0)
                {
                   cout<<"GPA can't be so high"<<endl;
                }
                else
                    cout<<"courent GPA is "<<GPA<<endl;
            }
            else if (j==2)
            {
                cout<<"How much to decrese the GPA?"<<endl;
                cin>>increse;
                double GPA=student.decreaseGPA(increse);
                if (GPA==0)
                {
                    cout<<"GPA can't be so low"<<endl;
                }
                else
                    cout<<"courent GPA is "<<GPA<<endl;
            }
            cout<<endl;
        }
        cout<<"to add info enter 1\nto print info enter 2\nto change stage enter 3\nto change GPA enter 4\nto end enter 0"<<endl;
        GPACheck(student);
        cin>>i;
        cout<<endl;
        
    }
    return student;//returning the class( in order to update it, didm't update whith any other method)
}


int main()
{
    Student student1;//initializing students
    Student student2;
      cout<<"to add/change info about student 1 enter 1\nto add/change info about student 2 enter 2\nto end enter 0"<<endl;
    int i;
    cin>>i;//picking which one to change
    cout<<endl;
    while (i!=0){//while not 0 scan for students
    if (i==1)
    student1=studentInfo(student1);
        else if (i==2)//changing them
    student2=studentInfo(student2);
    cout<<"to add/change info about student 1 enter 1\nto add/change info about student 1 enter 1\nto end enter 0"<<endl;
    cin>>i;//picking which one to change
    cout<<endl;

    }
    
    return 0;
}
