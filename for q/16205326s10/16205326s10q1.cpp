//
//  16205326s10q1.cpp
//
//  the program shows the use of funtions of different type but the same name
//  Created by Mikhail Yankelevich on 17/04/2018.


#include <iostream>

using namespace std;


int addTwo (int x, int y)
{
    cout<<"the summ of int x = "<< x<<" and int y = "<<y<<" equals to ";
    return x+y;
}

float addTwo (float x, float y)
{
    cout<<"the summ of float x = "<< x<<" and float y = "<<y<<" equals to ";
    return x+y;
}

double addTwo (double x, double y)
{
    cout<<"the summ of double x = "<< x<<" and double y = "<<y<<" equals to ";
    return x+y;
}

long addTwo (long x, long y)
{
    cout<<"the summ of long x = "<< x<<" and long y = "<<y<<" equals to ";
    return x+y;
}
int main()
{
    int i=4, j=4;
    double ii=4,jj=4.7;
    float iii=4.5,jjj=4;
    long iiii=4,jjjj=4;
    cout <<addTwo(i,j);//adding int numbers
    cout<<endl;
    cout <<addTwo(ii,jj);//adding two double numbers
    cout<<endl;
    cout <<addTwo(iii,jjj);//adding two float numbers
    cout<<endl;
    cout <<addTwo(iiii,jjjj);//adding two long numbers
    cout<<endl;
    
}
