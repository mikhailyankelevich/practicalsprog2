//
//  16205326s10q2.cpp
//  
//  the program shows the use of the functions fo different type
//  it recognise when different value type is entered and prints what type it was
//  Created by Mikhail Yankelevich on 17/04/2018.
//


#include <iostream>

using namespace std;

int printData(int x)
{
    cout<<"the entered int x is ";
    return x;
}
char printData(char x)
{
    cout<<"the entered char x is ";
    return x;
}
double printData(double x)
{
    cout<<"the entered double x is ";
    return x;
}

int main()
{
    cout<< "enter int, double and  char , and the program will find what type was entered";
    cout <<endl;
    int i; double j; char c;
    cout<< "enter int"<<endl;//asking for values
    std::cin>> i;
    cout << printData(i);//recignising them
    cout <<endl;
    cout<< "enter double"<<endl;
    std::cin>> j;
    cout << printData(j);
    cout <<endl;
    cout<< "enter char"<<endl;
    std::cin>> c;
    cout << printData(c);
    cout <<endl;
}



