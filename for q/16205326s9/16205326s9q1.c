//
//  16205326s9q1.c
//
//  Created by Mikhail Yankelevich on 10/04/2018.
//
//

#include <stdio.h>

struct zip_custs{//initializing the struct
    int zip_code;
    int customer_count;
};

int main(void)
{
    FILE *customers;
    customers= fopen("customers.dat","a+");//openning the file
    
    int myCustomers [5][2] = {{86956, 1}, {36568, 3}, {6565, 0}, {999555, 22}, {85446, 88}};
    struct zip_custs cutomers0={myCustomers[0][0], myCustomers[0][1]};//setting customers array with zip code in the first value and count in the second
    struct zip_custs cutomers1={myCustomers[1][0], myCustomers[1][1]};
    struct zip_custs cutomers2={myCustomers[2][0], myCustomers[2][1]};
    struct zip_custs cutomers3={myCustomers[3][0], myCustomers[3][1]};
    struct zip_custs cutomers4={myCustomers[4][0], myCustomers[4][1]};
    
    
    fprintf(customers,"ZIP     COUNT     \n\n");
    fprintf(customers,"%-8d%-10d\n", cutomers0.zip_code, cutomers0.customer_count);//prinitng out the result in the file
    fprintf(customers,"%-8d%-10d\n", cutomers1.zip_code, cutomers1.customer_count);
    fprintf(customers,"%-8d%-10d\n", cutomers2.zip_code, cutomers2.customer_count);
    fprintf(customers,"%-8d%-10d\n", cutomers3.zip_code, cutomers3.customer_count);
    fprintf(customers,"%-8d%-10d\n", cutomers4.zip_code, cutomers4.customer_count);

    
    
    return 0;
}
