//
//  16205326s8q2.c
//
//  Created by Mikhail Yankelevich on 03/04/2018.
// 
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct county{//initializing structs and functions
    char *longName;
    char *shortName;
    unsigned int population;
};
struct town{
    char *name;
    unsigned int population;
    struct county aCounty;
};

void getTown(FILE *town,FILE *county);
void getCounty(FILE *county, struct county c);


int main(void)
{
    FILE *town;
    FILE *county;
    town=  fopen("/Users/macbook/Downloads/towns.txt", "r");//openning the town file fro reading
    
    county= fopen("/Users/macbook/Downloads/counties.txt", "r");//openning the countries file for reading
    printf ("%-15s     %-10s      %-15s       %-10s      %-10s\n\n", "Town Name","Town Pop.", "County", "Short name", "County pop."   );//printing out the header of the table
    getTown(town, county);
    
    return 0;
}


void getTown(FILE *town, FILE *county)
{
    struct town t;
    
    char acounty[15];
    char name[15];
    

    while(!feof(town))//scanning for struct data
    {
        fscanf(town,"%s%u%s", name, &t.population, acounty);

        t.name=name;
        t.aCounty.longName= acounty ;
        
        printf ("%-15s     %-10u      %-15s  ", t.name, t.population,  t.aCounty.longName);//printing out the town
        
        
        getCounty(county, t.aCounty );//looking for the same county in another file
    }
}

void getCounty (FILE *county, struct county c)
{
    char longName[15];
    char shortName[5];
    fseek( county, 0, SEEK_SET );
    while(!feof(county))//scanning for data and comparing it with the passed name
    {
         fscanf(county,"%s%s%u", longName,shortName, &c.population);\
        if (!strcmp(longName, c.longName))
        {
            c.shortName= shortName;
            printf ("     %-10s      %-10u\n",c.shortName, c.population);//printing out the county
            break;
            
        }
    }
    
}
