//
//  16205326s8q3.c
//
//
//  Created by Mikhail Yankelevich on 05/04/2018.


#include <stdio.h>

void *calloc(size_t nmemb, size_t size);//initializing functions , calloc and realoc
void *realloc(void *ptr, size_t size);

void forInt(int num);
void forFloat(int num);

int main(void)
{
    printf("enter the type (1- int, 2- float):\n" );//letting person to chose between int and float
    short i;
    scanf("%hd", &i);
    printf("enter the number of elements you whish to store:\n");
    int num;
    scanf("%d", &num);
    if ( i==1)
        forInt(num);//in case of the person chosing int
    else if (i==2)
        forFloat(num);//in case of float
    
    return 0;
}

void forInt (int num)
{
    int *intArray;
    intArray= calloc(num, sizeof(int));//alocating the memory
    float sum=0;
    
    printf("enter values you want to store:\n");
    for(int i = 0; i < num; i++)//scanning the value
    {
        scanf("%d", intArray + i);
        sum=sum + *(intArray + i);//calculating the sum
    }
    float average= sum/num;//calculating the average
    printf(" the average is: %.2f\n",average);//printing out the average

    
    
    
    
    int i, numNew=0;
    do
    {
        printf("to enter more values enter 1, else enter 0: ");//checking if person wants to ad more
        scanf("%d", &i);
        
        if (i==1)
        {
            printf("how many number do you wish to add?:");//if yes , how many
            int numPlus;
            scanf("%d", &numPlus);
            
            
            numNew=num+numPlus;//chenging overall number of elements
            
            intArray= realloc(intArray,numNew*(sizeof(int)));//realocating the memory
            
            
            printf("enter values you want to store:\n");
            for( int j= num; j<numNew; j++)//getting values
            {
                scanf("%d", intArray + j);
                sum=sum + *(intArray + j);
            }
            average= sum/numNew;//calculating average
            printf("the average is: %.2f\n", average);
            
        }
    }while(i==1);

}


void forFloat (int num)//doint the same as with int but for float
{
    float *intArray;
    intArray= calloc(num, sizeof(int));
    float sum=0;
    
    printf("enter values you want to store:\n");
    for(int i = 0; i < num; i++)
    {
        scanf("%f", intArray + i);
        sum=sum + *(intArray + i);
    }
    float average= sum/num;//calculating the average
    printf(" the average is: %.2f\n",average);
    
    
    
    
    
    int i, numNew=0;
    do
    {
        printf("to enter more values enter 1, else enter 0: ");
        scanf("%d", &i);
        
        if (i==1)
        {
            printf("how many number do you wish to add?:");
            int numPlus;
            scanf("%d", &numPlus);
            
            
            numNew=num+numPlus;
            
            intArray= realloc(intArray,numNew*(sizeof(int)));
            
            
            printf("enter values you want to store:\n");
            for( int j= num; j<numNew; j++)
            {
                scanf("%f", intArray + j);
                sum=sum + *(intArray + j);
            }
            average= sum/numNew;
            printf("the average is: %.2f\n", average);
            
        }
    }while(i==1);
}
